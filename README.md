Task

Having **JoinOperation interface**, provide three implementations of this interface:

Java Task

`public interface JoinOperation<D1, D2, R> {
Collection<R> join(Collection<D1> leftCollection, Collection<D2> rightCollection);
}`

1 - InnerJoinOperation

2 - LeftJoinOperation

3 - RightJoinOperation

D1 - is a generic type of the elements in the left collection

D2 - is a generic type of the elements in the right collection

R - is a generic type of the elements in the resulting collection

For the _InnerJoinOperation, LeftJoinOperation and RightJoinOperation_ create two classes that hold the data:

`DataRow<K extends Comparable<K>, V>`, where 

K is a generic type of the key, 

V is a generic type of the value.

`JoinedDataRow<K extends Comparable<K>, V1, V2>`, where 

K is a generic type of the key, 

V1 and V2 are generic types of the values

For our task, DataRow<K, V> is your D1/D2 and 

JoinedDataRow<K, V1, V2> is R in the implementation classes. 

IMPORTANT: Do not change JoinOperation interface!


So, the signature in the InnerJoinOperation, LeftJoinOperation, and RightJoinOperation should look like the next:


`Collection<JoinedDataRow<K, V1, V2>> join(Collection<DataRow<K, V1>> leftCollection, Collection<DataRow<K, V2>> rightCollection)`;

Join should be performed by key: _K_
From an algorithm point of view:
For simplicity, we can say there are no duplicated keys in each separate collection.
The collections are always sorted.
Consider the solution efficiency for high-volume input.
Create JUnit tests for all implementations.
Junit test should be executed by `mvn test` command.