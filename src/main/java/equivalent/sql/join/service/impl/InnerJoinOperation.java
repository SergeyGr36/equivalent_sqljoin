package equivalent.sql.join.service.impl;

import equivalent.sql.join.data.DataRow;
import equivalent.sql.join.data.JoinedDataRow;
import equivalent.sql.join.service.JoinOperation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class InnerJoinOperation<V2, V1, K extends Comparable<K>> implements JoinOperation<DataRow<K, V1>, DataRow<K, V2>, JoinedDataRow<K, V1, V2>> {
    @Override
    public Collection<JoinedDataRow<K, V1, V2>> join(Collection<DataRow<K, V1>> leftCollection, Collection<DataRow<K, V2>> rightCollection) {
        List<JoinedDataRow<K, V1, V2>> result = new ArrayList<>();
        rightCollection.forEach(l -> {
            leftCollection.stream()
                    .filter(r -> r.getKey().equals(l.getKey()))
                    .forEach(r -> result.add(new JoinedDataRow(
                            r.getKey(), r.getValue(), l.getValue())));
        });
        return result;
    }
}
