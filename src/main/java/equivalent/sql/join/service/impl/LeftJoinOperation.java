package equivalent.sql.join.service.impl;

import equivalent.sql.join.data.DataRow;
import equivalent.sql.join.data.JoinedDataRow;
import equivalent.sql.join.service.JoinOperation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class LeftJoinOperation<V2, V1, K extends Comparable<K>> implements JoinOperation<DataRow<K, V1>, DataRow<K, V2>, JoinedDataRow<K, V1, V2>> {
    @Override
    public Collection<JoinedDataRow<K, V1, V2>> join(Collection<DataRow<K, V1>> leftCollection, Collection<DataRow<K, V2>> rightCollection) {
        List<JoinedDataRow<K, V1, V2>> result = new ArrayList<>();
        leftCollection.forEach(l ->
            result.add(new JoinedDataRow(l.getKey(), l.getValue(), rightCollection.stream()
            .filter(r-> l.getKey().equals(r.getKey()))
            .findAny().orElse(null)))
        );
        return result;
    }
}
