package equivalent.sql.join.service;

import equivalent.sql.join.data.DataRow;
import equivalent.sql.join.service.impl.InnerJoinOperation;
import equivalent.sql.join.service.impl.LeftJoinOperation;
import equivalent.sql.join.service.impl.RightJoinOperation;

import java.util.Arrays;
import java.util.Collection;

public class CallGenerator {
    public void run(){
        Collection<DataRow<Integer, String>> left = Arrays.asList(
                new DataRow<>(0, "Ukraine"),
                new DataRow<>(1, "Germany"),
                new DataRow<>(2, "France"));
        Collection<DataRow<Integer, String>> right = Arrays.asList(
                new DataRow<>(0, "Kiev"),
                new DataRow<>(1, "Berlin"),
                new DataRow<>(3, "Budapest"));
        System.out.println("Inner Join = "+new InnerJoinOperation().join(left, right));
        System.out.println("Left Join = "+new LeftJoinOperation().join(left, right));
        System.out.println("Right Join = "+new RightJoinOperation().join(left, right));
    }
}
