package equivalent.sql.join;

import equivalent.sql.join.service.CallGenerator;

public class Runner
{
    public static void main( String[] args )
    {
        CallGenerator callGenerator = new CallGenerator();
        callGenerator.run();
    }
}
